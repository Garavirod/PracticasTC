class AFDI
		
	def initialize()
		@alfa=[]
		' '.upto('}'){|i| @alfa.push(i)}
		@hexa = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
		@decimall = @hexa-['A','B','C','D','E','F']
		@octal = @hexa - ['8','9','A','B','C','D','E','F']
		@spaces = ["\n","\t","\r"," "]
		@operators = ['+','-']
		@terminacion = [';','(',')','{','}']
		@beta = @alfa-@operators-@decimall-['.',';']-@spaces
	end

	def sta0(c)
		if ((@beta-['<','>','='])+@spaces+@terminacion+@decimall+['.',',']).any? { |e| e==c }
			return 0
		elsif c == '=' 
			return 11
		elsif c == '<' or c == '>'
			return 12
		elsif @operators.any? {|e| e==c}
			return 4
		else
			return -1
		end
	end

	def sta1(c)
		if c == 'x'
			return 2
		elsif (@octal).any? { |e| e==c }
			return 3
		elsif c == '.'
			return 6
		elsif (@terminacion+@spaces+[',']).any? { |e| e==c }
			return 0
		else
			return -1				
		end
	end

	def sta2(c)
		if @hexa.any? { |e| e==c }
			return 9
		else
			return -1
		end
	end

	def sta3(c)
		if @octal.any? { |e| e==c}
			return 3
		elsif (@terminacion+@spaces+[',']).any? { |e| e==c }
			return 0
		else		
			return -1	
		end
	end

	def sta4(c)
		if c == '0'
			return 1
		elsif (@decimall-[0]).any? { |e| e==c }
			return 5
		elsif (@operators+@beta).any? { |e| e==c  }
			return 0																				
		else
			return -1
		end
	end


	def sta5(c)
		if c == 'E'
			return 7
		elsif @decimall.any? { |e| e == c }
			return 5		
		elsif c == '.'
			return 6
		elsif (@terminacion+@spaces+[',']).any? { |e| e==c }
			return 0
		else
			return -1
		end
	end

	def sta6(c)
		if  @decimall.any? { |e| e == c }
			return 10
		else
			return -1
		end
	end

	def sta7(c)
		if @operators.any? { |e| e==c }
			return 8
		else
			return -1
		end
	end

	def sta8(c)
		if @decimall.any? { |e| e==c }
			return 10
		else
			return -1
		end
	end

	def sta9(c)
		if @hexa.any? { |e| e==c }
			return 9
		elsif (@terminacion+@spaces+[',']).any? { |e| e==c }
			return 0
		else
			return -1	
		end
	end

	def sta10(c)
		if  @decimall.any? { |e| e == c }
			return 10
		elsif c == 'E'
			return 13	
		elsif (@terminacion+@spaces+[',']).any? { |e| e==c }
			return 0
		else
			return -1
		end
	end

	def sta11(c)
		
		if c == '0'
			return 1
		elsif (@decimall-[0]).any? { |e| e==c}
			return 5
		elsif @operators.any? { |e| e==c}
			return 4			
		elsif (@spaces).any? { |e| e==c }
			return 11
		elsif @beta.any? { |e| e==c }
			return 14						
		else
			return -1
		end
	end	

	def sta12(c)
		if (@decimall-['0']).any? { |e| e==c }
			return 5
		elsif c=='0'
			return 1
		elsif (@beta+@spaces).any? { |e| e==c}
			return 0	
		else
			return -1
		end
	end

	def sta13(c)
		if @operators.any? { |e| e==c }
			return 8
		else
			return -1
		end
		
	end


	def sta14(c)
		if @operators.any? { |e| e==c }
			return 4
		elsif (@beta+@spaces).any? { |e| e==c }
			return 14
		else
			return -1
		end
		
	end
	

	def dameEstado(q1,c)
		_Q ={
			0 => sta0(c),
			1 => sta1(c),
			2 => sta2(c),
			3 => sta3(c),
			4 => sta4(c),
			5 => sta5(c),
			6 => sta6(c),
			7 => sta7(c),
			8 => sta8(c),
			9 => sta9(c),
		   10 => sta10(c),
		   11 => sta11(c),
		   12 => sta12(c),
		   13 => sta13(c),
		   14 => sta14(c)}
		 _Q[q1]
	end

	public
	def reconocer(cadena,line)
		q = 0
		cadena.each_char do |i|
			q = dameEstado(q,i)
			if q ==-1
				puts " > ERROR EN LA LINEA #{line}"
				break
			end
		end
	end
end

# creamos un obj de la clase ADFI
obj = AFDI.new()

#Contador de líneas del archivo
num_line = 0
#Leemos el archivo Numeros.java

File.foreach("Numeros.java") do |line| 
	#usamos el métdo de clase que valida la cadena de cada linea del archivo
	num_line +=1
	obj.reconocer(line,num_line)

end

