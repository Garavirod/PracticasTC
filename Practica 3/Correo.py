from sys import stdin, stdout

def stat1(c):
	if c == '@':
		return 2
	else:
		return 1

def stat2(c):
	if c == '.':
		return 0
	elif c == '@':
		return 0
	else:
		return 3

def stat3(c):
	if c == '.':
		return 4
	elif c == '@':
		return 0
	else:
		return 3

def stat4(c):
	if c == 'c':
		return 5
	else:
		return 0

def stat5(c):
	if c == 'o':
		return 6
	else:
		return 0

def stat6(c):
	if c == 'm':
		return 7
	else:
		return 0

def DameEstado(q,c):

	opc={
		1 : stat1,
	    2 : stat2,
	    3 : stat3,
	    4 : stat4,
	    5 : stat5,
	    6 : stat6,   
	}
	return opc[q](c)		
		
def recononcer(cadena):
	q=1
	band = False
	for i in cadena:
		q=DameEstado(q,i)

		if q == 0 or q == 1:
			band = False
		if q == 7:
			band = True
	if band:
		stdout.write("\nE-MAIL VÁLIDO\n")
	else:
		stdout.write("\nE-MAIL NO VÁLIDO\n")						


if __name__ == '__main__':

	stdout.write("\nIngrese su e-mail:\n->")
	recononcer(stdin.readline()[:-1])