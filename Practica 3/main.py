from sys import stdin,stdout
operadores=['.','+','-']

def EsDigito(c):
	try:
		int(c)
		return True
	except ValueError:
		return False


def sta1(c):
	if EsDigito(c):
		return 2 
	else:
	 	return 0 

def sta2(c):
	if EsDigito(c):
		return 2
	elif c in operadores:
		return 3
	elif c == 'E':
		return 5
	else:
		return 0
def sta3(c):
	if EsDigito(c):
		return 4
	else:
		return 0

def sta4(c):
	if EsDigito(c):
		return 4
	elif c== 'E':
		return 5
	else:
		return 0	

def sta5(c):
	if EsDigito(c):
		return 7
	elif c in operadores:
		return 6
	else:
		return 0

def sta6(c):
	if EsDigito(c):
		return 7 
	else:
	 	return 0
def sta7(c):
	if EsDigito(c):
		return 7
	else:
		return 0	 				
			 

def DameEstado(q,c):

	opc={
		1 : sta1,
	    2 : sta2,
	    3 : sta3,
	    4 : sta4,
	    5 : sta5,
	    6 : sta6,
	    7 : sta7,   
	}
	return opc[q](c)		
		
def recononcer(cadena):
	q=1
	for i in cadena:
		q=DameEstado(q,i)
		if q == 0:
			stdout.write("NO ACEPTADA\n")
			break
	if q == 7 or q == 4:
		stdout.write("\nACEPTADA\n")

if __name__ == '__main__':

	recononcer(stdin.readline()[:-1])
