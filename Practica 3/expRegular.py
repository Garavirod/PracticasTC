from sys import stdin,stdout

def Nuevo_estado(Qs,q):
	try:
		return Qs[q]
	except KeyError:
		return 0 

def sta1(car):
	newQ={"a":2, "c":3}
	return Nuevo_estado(newQ,car)

def sta2(car):
	newQ={'c':3}
	return Nuevo_estado(newQ,car)

def sta3(car):
	newQ={'b':5,'a':4}
	return Nuevo_estado(newQ,car)	

def sta4(car):
	newQ={'a':4, 'b':4,'c':3}
	return Nuevo_estado(newQ,car)

def sta5(car):
	newQ={'b':5, 'a':5, 'c':5}
	return Nuevo_estado(newQ,car)		
			 

def DameEstado(q,c):

	opc={
		1 : sta1,
	    2 : sta2,
	    3 : sta3,
	    4 : sta4,
	    5 : sta5,
	}
	return opc[q](c)		
		
def recononcer(cadena):
	q=1
	for i in cadena:
		q=DameEstado(q,i)
		if q == 0:
			stdout.write("NO ACEPTADA\n")
			break
	if q >=2 and q<= 5:
		stdout.write("\nACEPTADA\n")
			

if __name__ == '__main__':

	recononcer(stdin.readline()[:-1])
