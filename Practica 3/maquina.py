from sys import stdin,stdout
monedas=[10,5,2]

def MuestraEdo(q0,mon,qr):
		stdout.write("-"*55+"\n")
		stdout.write('Edo actual'+'{:^10}'.format(""))	
		for i in monedas:
			stdout.write('|{:^10}'.format(i))	
		stdout.write("\n")
		stdout.write("-"*55+"\n")
		if mon>9:
			stdout.write('{:^2}'.format("")+"f(Q"+str(q0)+',{}){:^10}'.format(mon,""))
		else:
			stdout.write('{:^3}'.format("")+"f(Q"+str(q0)+',{}){:^10}'.format(mon,""))
		for i in monedas:
			if i==mon:
				stdout.write('|{:^10}'.format("Q{}".format(qr)))
			else:
				stdout.write('|{:^10}'.format(""))
		stdout.write("\n")
		stdout.write("-"*55+"\n")		

def Nuevo_estado(q,Qs):

	try:
		return Qs[q]
	except KeyError:
		return 0 

def sta1(c):
	newQ={10:2,5:3,2:4}
	return Nuevo_estado(c,newQ) 

def sta2(c):
	newQ={10:5,5:5,2:5}
	return Nuevo_estado(c,newQ) 

def sta3(c):

	newQ={2:6,10:5,5:2}
	return Nuevo_estado(c,newQ) 


def sta4(c):
	newQ={5:6,2:7,10:5}
	return Nuevo_estado(c,newQ) 
	

def sta6(c):
	newQ={5:5,10:5,2:9}
	return Nuevo_estado(c,newQ) 

def sta7(c):
	newQ={2:8,5:9,10:5}
	return Nuevo_estado(c,newQ) 			


def sta8(c):
	newQ={2:9,5:10,10:5}
	return Nuevo_estado(c,newQ) 

def sta9(c):

	newQ={10:5,5:5,2:10,}
	return Nuevo_estado(c,newQ) 

def sta10(c):
	return sta2(c)
			 

def DameEstado(q,c):

	opc={1 : sta1,2 : sta2,3 : sta3,4 : sta4, 6 : sta6,7 : sta7,8 : sta8,9 : sta9,10 : sta10}
	return opc[q](c)		
		
def recononcer():
	q=1
	while True:
		stdout.write("Insert coin > $: \n")
		moneda = int(stdin.readline())
		MuestraEdo(q,moneda,DameEstado(q,moneda))
		q=DameEstado(q,moneda)
		if q==5:
			stdout.write("DISFRUTA\n")
			break
		elif q == 0:
			stdout.write("NO ACEPTADO\n")
			break	

if __name__ == '__main__':
	recononcer()

			