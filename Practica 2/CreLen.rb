class CreLen
	
	def initialize()	
		@alfabeto=[]
		@Leng=[]
		@@a=0
	end

   	
   def Muestra(*len)
	   	  print "*"*60+"\n"
		  puts "lenguaje/s Resultante/s".center(60," ")
		  print "*"*60+"\n\n"
		  len.each do |i|
		  print "\n L = {"
		  print i*','
		  print "}\n\n"
		end
	end

	def muestraSL(*len)
		  len.each do |i|
		  print "\n L = {"
		  print i*','
		  print "}\n\n"
		end
	end
    
	def GeLen(alfabeto)	
	  	@Leng=[]
		@@a +=1
		@alfabeto = alfabeto
		print "*"*60+"\n"
	    puts "Generar lenguaje L#{@@a}".center(60," ")
	    print "*"*60+"\n\n"
	    print "Num de palabras >: "
	    pal = gets.chomp
	    pal = pal.to_i
	    print "Num de simbolos >: "
	    long = gets.chomp
	    print "\n\n"
	    long = long.to_i
  		return CreaLen(@alfabeto,pal,long)
	end

	
	def self.EliminaRep(len)
		tHash = Hash[len.collect{ |val| [val, ""] } ]
		leng = tHash.keys()		
		return leng
	end
	
	def CreaLen(sig,pal,long)
		word =''
		a =sig.length
		@Leng=[]
	  	pal.times do
	  		long.times{word<<"#{sig[rand(a)]}"}
	  		@Leng.push(word)
	  		word =''
  		end

  		if CreLen.EliminaRep(@Leng).length < @Leng.length
  			return CreaLen(sig,pal,long)
  		else
  			return @Leng
  		end	
	end

	
end