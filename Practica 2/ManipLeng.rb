require_relative('CreLen.rb')
class ManipLeng

  public
  def initialize()
  end

  public
  def self.Union(*lgs)
    lu =[]
    lgs.each{|i| lu +=i}
    return lu
  end

  def self.Diferencia(len1,len2)
    dif=[]
    my_hash = Hash[len2.collect { |cve| [cve, ""] } ]
    len1.each do|i| 
      if my_hash[i]==nil
        dif.push(i)
      end
    end
    return dif    
  end
  
  def self.Potencia_Leng(len,n)
    lp=[]
    if n==0
        lp =["λ"]
    elsif n==1
        lp=len
    elsif n<0 
        lp =len.collect{|i| "#{(i.reverse)}"*(n*-1)}
    else
        lp =len.collect{ |i| "#{i}"*n }
    end
    return lp
  end

  public
  def self.Concat(l1,l2)
    lc=[]
    l1.each {|i| l2.each {|j| lc.push("#{i}"+j)}}
    return lc
  end

  public
  def self.Concat_Len(*lgs)
    lco =[]
    lco =lgs[0]
    k = (lgs.length)-1
      for i in 1..k
        lco =ManipLeng.Concat(lco,lgs[i])
      end
    return lco
  end


end



class PlacaAuto < ManipLeng
     @lp=["AL",
          "A",
          "AB",
          "AC",
          "AM",
          "AS",
          "AV",
          "B",
          "BA",
          "BR",
          "BU",
          "CA",
          "CD",
          "CN",
          "CS",
          "CR",
          "CO",
          "CU",
          "CE",
          "GE",
          "GR",
          "GU",
          "GI",
          "HU",
          "H",
          "BL",
          "J",
          "L",
          "LE",
          "LU",
          "M",
          "MA",
          "ML",
          "MU",
          "N",
          "O",
          "P",
          "PA",
          "PO",
          "R",
          "S",
          "SE",
          "SV",
          "SO",
          "T",
          "TE",
          "TR",
          "TO",
          "V",
          "VA",
          "VI",
          "ZA",
          "Z",
          "C",
          "ME"]

          @ld=['0','1','2','3','4','5','6','7','8','9']
          @ll=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','R','S','T','U','V','W','X','Y','Z']
          @lg=['-']
          
          @lenguaje =CreLen.new
      

          
  def initialize()
  end

  private
  def self.GenPlaca(n)
      if n==1
        #Placa Antigüa
        lp = @lenguaje.CreaLen(@lp,1,1)
        ld = @lenguaje.CreaLen(@ld,1,6) 
        return ManipLeng.Concat_Len(lp,@lg,ld)
      else
        #Placa Nueva
        lp = @lenguaje.CreaLen(@lp,1,1)
        ld = @lenguaje.CreaLen(@ld,1,4)
        ll = @lenguaje.CreaLen(@ll,1,2) 
        return ManipLeng.Concat_Len(lp,@lg,ld,@lg,ll)
      end
  end
    
    private
    def self.valida(len)
      a =len.length-1
      if len[a] == 'A'
        return false
      else
        return true
      end
    end

    public
    def self.DamePlaca(n)
      lm =[]
      lm = PlacaAuto.GenPlaca(n)
      if PlacaAuto.valida(lm)
        return lm
      else
        return PlacaAuto.DamePlaca(rand(2))
      end
    end
  
end
