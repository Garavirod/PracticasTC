class Alfabeto
		
  def initialize()
  @alfabeto=[]
	end

	def Gen_Alfabeto(opc)
      if opc==1
        return Opcion_1()
      else
        return Opcion_2()
      end  
  end

   public
   def Muestra_Alfabeto()
    print "*"*60+"\n"
    puts "Alfabeto Resultante".center(60," ")
    print "*"*60+"\n\n"
    print "\n Σ = {"
    print @alfabeto*","
    print "}\n\n"
   end

    public
    def Opcion_1()
    print "*"*60+"\n"
    puts "Introducir el rango de su alfabeto".center(60," ")
    print "*"*60+"\n\n"
    print "Desde >: "
    x=gets.chomp
    print "Hasta >: "
    y=gets.chomp
    x.upto(y){|i| @alfabeto.push(i)}
    return @alfabeto
    system('clear')
    end

    public
    def Opcion_2()
    print "*"*60+"\n"
    puts "Introducir su alfabeto caracter por caracter".center(60," ")
    print "*"*60+"\n\n"
    print "Para terminar introducir -1\n\n"
    begin
      print "Introduce caracter >: "
      c = gets.chomp
      if c != "-1"
        @alfabeto.push(c)
      end
    end until c == "-1"
      return @alfabeto
      system("clear")
  end
	
end