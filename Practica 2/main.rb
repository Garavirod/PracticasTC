require_relative("ManipLeng.rb")
require_relative("Alfabeto.rb")
require_relative("CreLen.rb")
# require_relative("PlacaAuto.rb")

#Creamos un obj de la Clase Alfabeto
alfabeto = Alfabeto.new()
begin
	print "\n\nIngresar alfabeto:\n\n"
	print " 1.- Rango letra - letra\n"
	print " 2.- Caracter a caracter\n"
	print "\n\n opc >: "
	opc=gets.chomp
	system("clear")
	case opc

	when "1" then
		#De la instancia Alfabeto accedemos al método que pide el alfabeto
		sigma = alfabeto.Gen_Alfabeto(1)
				alfabeto.Muestra_Alfabeto()
	when "2" then
		sigma = alfabeto.Gen_Alfabeto(2)
				alfabeto.Muestra_Alfabeto()
	else
		print"\n Opcion invalida... \n\n"
	end
end until opc == "1" or opc == "2"		

#Creamos los leng L1 y L2 bajo el alfabeto sigma
Lenguaje=CreLen.new

l1 = Lenguaje.GeLen(sigma)
	 Lenguaje.Muestra(l1)
l2 = Lenguaje.GeLen(sigma)
	 Lenguaje.Muestra(l2)

#Creamos un Obj de la clase Lenguajes, que manipule a los lenguajes de argumentos
# y le pasamos como parametro los lenguajes que manipulará
# ManipLeng = Lenguajes.new()

	#Con el obj maniLen de la calse Lenguajes llamaremos a los métodos de la clase que
	#se encarguen de manipular los lenguajes que se le pasaraon por parametro L1 L2

	begin
		print "\n\n Operaciones para los Lenguajes:\n\n"
		Lenguaje.muestraSL(l1,l2)
		print " 1.- Union\n"
		print " 2.- Concatenacion\n"
		print " 3.- Diferencia\n"
		print " 4.- Potencia\n"
		print " 5.- Generar Placa\n"
		print " 6.- Salir\n"
		print "\n\n opc >: "
		opc2 =gets.chomp
		system("clear")
		case opc2

		when "1" then
			
		#Mustramos la union de L1 con L2
		Lenguaje.Muestra(CreLen.EliminaRep(ManipLeng.Union(l1,l2)))
		
		when "2" then

		#Muestra la concatenacion de L1 con L2
		Lenguaje.Muestra(ManipLeng.Concat_Len(l1,l2))

		when "3" then 

		#Mostramos las diferencia de L1 con L2 y viceversa
		Lenguaje.Muestra(ManipLeng.Diferencia(l1,l2),ManipLeng.Diferencia(l2,l1))
		# Lenguaje.Muestra(ManipLeng.Diferencia(l2,l1))
		
		when "4" then 

			#Mostramos la potencia de un lenguaje L, pedimos que se seleccione qué lenguaje
			begin
				print "\n Slecciona el Lenguaje para elever a un potencia:\n\n"
				print "\n 1.- L1\n"
				print "\n 2.- L2\n"
				print "\n\n Opc >: "
				opc =gets.chomp
					begin
						print "\n\nPotencia n >:"
						n =gets.chomp
						n = n.to_i
						system("clear")
					end until n>= -5 and n<=5 

				case opc
					when "1" then
						Lenguaje.Muestra(ManipLeng.Potencia_Leng(l1,n))
					when "2" then 
						Lenguaje.Muestra(ManipLeng.Potencia_Leng(l2,n))
					else
						print "\n\n¡Opcion invalida!\n"
				end
			end until opc == "1" or opc =="2" 	

		when "5" then 
			Lenguaje.Muestra(PlacaAuto.DamePlaca(rand(2)))
		when "6" then 	
			break
		else
			print"\n Opcion invalida... \n\n"
		end
	end until opc2 == "6"	
