#!/usr/bin/ruby
class AFNDII
	def initialize(str)
		@cadena = str
		@aceptacion = []
	end


	def staA(c)
		if c == '1' or c ==''
			return 'A'
		else
			return nil
		end
	end

	def staB
		hilo1 = Thread.new{validaI('A')}
		hilo2 = Thread.new{validaII('C',@cadena)}
		hilo1.join
		hilo2.join 
	end

	def staC(c,cad)
		if c == '0'
			hilo3 = Thread.new{validaII('D',cad)}
			hilo3.join
			return 'C'
		elsif c == '1'
			return 'C'
		end
	end

	def staD(c)
		if c ==''
			return 'D'
		else
			return nil
		end
	end

	def validaI(sta)
		q0 = sta
		@cadena.each_char do |chr|  
			q0 = DameEstado(q0,chr,'')
			if q0 == nil
				break
			end
		end
	end

	def validaII(sta,cad)
		q0 = sta
		cadTem = cad 
		cad.each_char do |chr|
			cadTem = (cadTem.reverse).chomp(cadTem[0])
			q0 = DameEstado(q0,chr,cadTem)
		end
	end

	def comenzar
		staB
	end


	def DameEstado(q,c,cad)
		_Q ={'A' => staA(c),'B' => staB,'C' => staC(c),'D' => staD(c)}
		_Q[q]
	end
	
end

print "Introduce cadena >: "
str = gets.chomp
obj = AFNDII.new(str)
obj.comenzar


