require_relative('Alfabeto.rb')
	sigma1=[]
	sigma2=[]
	begin
	print "\n\nIngresar alfabeto:\n\n"
	print " 1.- Rango letra - letra\n"
	print " 2.- Caracter a caracter\n"
	print "\n\n opc >: "
	opc=gets.chomp
	system("clear")
	case opc

	when "1" then
		print "Alfabeto 1: \n\n"
		sigma1 = Gen_Alfabeto(1)
		Muestra_Alfabeto(sigma1)
		print "\nAlfabeto 2: \n\n"
		sigma2 = Gen_Alfabeto(1)
		Muestra_Alfabeto(sigma2)
	when "2" then
		print "Alfabeto 1: \n\n"
		sigma1 = Gen_Alfabeto(2)
		print "\nAlfabeto 2: \n\n"
		sigma2 = Gen_Alfabeto(2)
	else
		print"\n Opcion invalida... \n\n"
	end
end until opc == "1" or opc == "2"

	#Pedimos cadenas al usario y las validamos
	w1 = Valida_Cadena(Pide_cadena(sigma1),sigma1)
	w2 = Valida_Cadena(Pide_cadena(sigma1),sigma1)

	#(w1w2)^n
	print Potencia_Cad(w1,w2)

	#|w1|x, donde x ԑ ∑
	print Concurrencia(w1)

	#indicar si w1 es prefijo o sufijo de w2 propio o no propio
	print "*"*60+"\n"
	puts "w1 (#{w1}) es prefijo o sufijo de w2 (#{w2}) propio o no ".center(60," ")
	print "*"*60+"\n\n"
	print Is_Pref(w1,w2)
	print Is_Suf(w1,w2)

	#∑1^n
	Muestra_Alfabeto(Potencia_Alfa(sigma1))

	#∑1 ∙ ∑2
  print "*"*60+"\n"
  puts "∑1 ∙ ∑2 ".center(60," ")
  print "*"*60+"\n\n"
	Muestra_Alfabeto(Concat_Alfa(sigma1,sigma2))

	#∑1 ∙ ∑2 ∙ ∑1
  print "*"*60+"\n"
  puts "∑1 ∙ ∑2 ∙ ∑1  ".center(60," ")
  print "*"*60+"\n\n"
	Muestra_Alfabeto(Concat_Alfa_Plus(sigma1,sigma2,sigma1))
