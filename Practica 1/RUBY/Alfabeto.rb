
  def Gen_Alfabeto(opc)
      if opc == 1
        return Opcion_1()
      else
        return Opcion_2()
      end
  end

  def Pide_cadena(alfabeto)
    print "*"*60+"\n"
    puts "Introducir cadena ".center(60," ")
    print "*"*60+"\n\n"
    print "Bajo el alfabeto "
    Muestra_Alfabeto(alfabeto)
    print "W >: "
    w = gets.chomp
    return w
  end

  def Muestra_Alfabeto(alfabeto)
    print " Σ = {"
    print alfabeto*","
    print "}\n\n"
  end

  def Valida_Cadena(w,alfabeto)
    system("clear")
    contador = 0
      w.each_char do |simb_cad|
        alfabeto.each do |simb_alf|
            if simb_cad == simb_alf
              contador +=1
              next
            end
        end
      end
      if contador < w.length
        print "\n¡Cadena '#{w}' es inavlida!\n\n"
        w = Pide_cadena(alfabeto)
        return Valida_Cadena(w,alfabeto)
      else
        print "\n¡La cadena '#{w}' es valida!\n\n"
        return w
      end
  end

  def Potencia_Cad(w1,w2)
    print "*"*60+"\n"
    puts "(w1w2)^n ".center(60," ")
    print "*"*60+"\n\n"
    print "Potencia n>: "
    n = gets.chomp
    n = n.to_i
    nuevacad = w1+w2
      if n > 0
        return "\n(#{w1}.#{w2})^#{n} = "+nuevacad*n+"\n"
      else
        n = n*-1
        nuevacad = nuevacad.reverse
        return "\n(#{w1}.#{w2})^#{n} = "+nuevacad*n+"\n"
      end
  end

  def Concurrencia(w)
    print "*"*60+"\n"
    puts "|w1|x, donde x ԑ ∑1".center(60," ")
    print "*"*60+"\n\n"
    print "Bajo la cadena '#{w}' "
    print "Simbolo >: "
    simb = gets.chomp #quita el salto de carr0
    concu = 0
    w.each_char do |chr|
      if simb==chr
        concu +=1
      end
    end
    return "\nLa concurrencia del símbolo '#{simb}' es #{concu}\n\n"
  end

  def Is_Pref(w1,w2)
    band = false
    unless w1 == w2
        for i in (0..(w2.length()-2))
            if w1 == w2[0..i]
                band=true
                break
            end
        end
      if band
        return "w1 no es un prefijo propio de w2\n\n"
      else
        return "w1 no es un prefijo propio de w2\n\n"
      end
    else
      return "w1 es prefijo no propio de w2\n\n"
    end
  end

  def Is_Suf(w1,w2)
    band = false
    unless w1 == w2
        for i in (0..(w2.length()-2))
            if w1 == w2[i..-1]
                band=true
                break
            end
        end
        if band
          return "w1 es un sufijo propio de w2\n\n"
        else
          return "w1 no es un sufijo propio de w2\n\n"
        end
    else
      return "w1 es un sufijo no propio de w2\n\n"
    end
  end

  def Concat_Alfa(alfa1,alfa2)
    nvo_alf=[]
    alfa1.each {|i| alfa2.each {|j| nvo_alf.push("#{i}"+j)}}
    return nvo_alf
  end

  def Concat_Alfa_Plus(*alfabetos)

    nuevo_alf=[]
    nuevo_alf=alfabetos[0]
    k = (alfabetos.length)-1
      for i in 1..k
        nuevo_alf=Concat_Alfa(nuevo_alf,alfabetos[i])
      end
    return nuevo_alf
  end

  def Potencia_Alfa(alfabeto)
   print "*"*60+"\n"
    puts "∑1^n ".center(60," ")
    print "*"*60+"\n\n"
    print " Potencia n>: "
    n = gets.chomp
    n = n.to_i
    nuevo_alf=[]
    nuevo_alf=alfabeto
    if n==0
      nuevo_alf=["λ"]
      return nuevo_alf
    elsif n==1
      return alfabeto
    elsif n<0
      n = n*-1
      for i in 1..(n-1)
        nuevo_alf=Concat_Alfa(nuevo_alf,alfabeto)
      end
      return nuevo_alf
    elsif n>0
      for i in 1..(n-1)
        nuevo_alf=Concat_Alfa(nuevo_alf,alfabeto)
      end
      return nuevo_alf
    end
  end

  def Opcion_1
    #agregamos al alfabeto los simbolos que el usuario estableció
    alfabeto=[]
    print "*"*60+"\n"
    puts "Introducir el rango de su alfabeto".center(60," ")
    print "*"*60+"\n\n"
    print "Desde >: "
    x=gets.chomp
    print "Hasta >: "
    y=gets.chomp
    x.upto(y){|i| alfabeto.push(i)}
    return alfabeto
    system('clear')
  end

  def Opcion_2
    alfabeto=[]
    print "*"*60+"\n"
    puts "Introducir su alfabeto caracter por caracter".center(60," ")
    print "*"*60+"\n\n"
    print "Para terminar introducir -1\n\n"
    begin
      print "Introduce caracter >: "
      c = gets.chomp
      if c != "-1"
        alfabeto.push(c)
      end
    end until c == "-1"

      system("clear")
      return alfabeto
  end
